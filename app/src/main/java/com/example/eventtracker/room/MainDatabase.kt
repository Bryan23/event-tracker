package com.example.eventtracker.room

import androidx.room.Database
import androidx.room.RoomDatabase
import com.example.eventtracker.Payout

@Database (entities = [Payout::class],version = 2, exportSchema = false)
abstract class MainDatabase: RoomDatabase(){
    abstract fun payoutDao(): PayoutDao
}