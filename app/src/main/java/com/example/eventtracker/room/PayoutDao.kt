package com.example.eventtracker.room

import androidx.room.*
import com.example.eventtracker.Payout

@Dao
interface PayoutDao {
    @Query("SELECT * FROM payout")
    fun getAll(): List<Payout>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(vararg payouts: Payout)

    @Delete
    fun delete(payout: Payout)

    @Query("DELETE FROM payout")
    fun deleteAll()
}