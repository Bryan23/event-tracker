package com.example.eventtracker.pushnotification

import android.util.Log
import com.example.eventtracker.Payout
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken

class EventTrackerFirebaseMessagingService : FirebaseMessagingService() {
    override fun onMessageReceived(remoteMessage : RemoteMessage) {
        super.onMessageReceived(remoteMessage)

        if (remoteMessage.data.isNotEmpty()) {
            val payoutData = remoteMessage.data["payout"].toString()

            val gson = Gson()
            val arrayOfPayout = object : TypeToken<Array<Payout>> () {}.type
            val payouts: Array<Payout> = gson.fromJson(payoutData, arrayOfPayout)

            NotificationUtil.buildPayoutNotification(payouts, this)
        }
    }

    override fun onDeletedMessages() {
        super.onDeletedMessages()
        Log.i("Firebase service", "Token deleted")
    }

    override fun onNewToken(newToken : String) {
        super.onNewToken(newToken)
        Log.i("Firebase service", "Token: $newToken")
    }

}