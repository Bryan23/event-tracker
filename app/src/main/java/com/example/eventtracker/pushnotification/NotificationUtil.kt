package com.example.eventtracker.pushnotification

import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.media.RingtoneManager
import android.util.Log
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import com.example.eventtracker.Payout
import com.example.eventtracker.PayoutActivity
import com.example.eventtracker.R


object NotificationUtil {
    const val CHANNEL_ID = "eventTracker"
    const val CHANNEL_NAME = "eventTracker"
    const val CHANNEL_DESCRIPTION = "Event Tracker App"
    private const val NOTIFICATION_ID = 1

    fun buildPayoutNotification(payoutList : Array<Payout>, context : Context) {

        val arrayListPayout = payoutList.toList() as ArrayList<Payout>

        val intent = Intent(context, PayoutActivity::class.java).apply {
            putParcelableArrayListExtra("payout", arrayListPayout)
            flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        }

        val pendingIntent =
            PendingIntent.getActivity(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT)

        val defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)
        val builder = NotificationCompat.Builder(context, CHANNEL_ID).apply {
            setSmallIcon(R.mipmap.ic_launcher_round)
            setSound(defaultSoundUri)
            setContentTitle("Payout Event")
            priority = NotificationCompat.PRIORITY_DEFAULT
            setContentIntent(pendingIntent)
            setAutoCancel(true)
        }

        with(NotificationManagerCompat.from(context)) {
            notify(NOTIFICATION_ID, builder.build())
        }
    }
}