package com.example.eventtracker

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.TextView
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.eventtracker.databinding.ItemPayoutBinding

class PayoutAdapter(private var eventList: List<Payout>, private val context : Context): RecyclerView.Adapter<PayoutAdapter.MyViewHolder>() {

    override fun onCreateViewHolder(parent : ViewGroup, viewType : Int) : MyViewHolder {
        val binding = DataBindingUtil.inflate<ItemPayoutBinding>(LayoutInflater.from(
            context
        ), R.layout.item_payout, parent, false)

        return MyViewHolder(binding, context)
    }

    override fun onBindViewHolder(holder : MyViewHolder, position : Int) {
        holder.bind(eventList[position])
    }

    override fun getItemCount() : Int {
        return eventList.size
    }

    inner class MyViewHolder(private val binding : ItemPayoutBinding, private val context : Context): RecyclerView.ViewHolder(binding.root) {
        fun bind (item: Payout) {
            val textViewFraud: TextView = binding.root.findViewById(R.id.tvFraudAlert)
            val textViewEventId: TextView = binding.root.findViewById(R.id.tvEventId)
            val textViewStatus: TextView = binding.root.findViewById(R.id.tvPayoutStatus)

            val fraudWarning: String = if (item.fraudAlert) {
                "Warning"
            } else {
                "Normal"
            }

            if (item.fraudAlert) {
                textViewFraud.setTextColor(context.resources.getColor(R.color.red))
            } else {
                textViewFraud.setTextColor(context.resources.getColor(R.color.green))
            }

            textViewStatus.text = item.status
            textViewFraud.text = fraudWarning
            textViewEventId.text = item.payoutId.toString()
        }
    }
}