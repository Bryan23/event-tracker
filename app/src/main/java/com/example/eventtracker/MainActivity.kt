package com.example.eventtracker

import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.room.Room
import com.example.eventtracker.pushnotification.NotificationUtil
import com.example.eventtracker.room.MainDatabase

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState : Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        registerNotificationChannel()
        val retrieveFromDb = Thread {
            val db = Room.databaseBuilder(
                applicationContext, MainDatabase::class.java, "main-database"
            ).build()

            val payoutDao = db.payoutDao()

            //retrieve all payout from db
            val payoutList : ArrayList<Payout> = payoutDao.getAll() as ArrayList
            if (payoutList.isNotEmpty()) {
                val intent = Intent(this, PayoutActivity::class.java).apply {
                    putParcelableArrayListExtra("payout", payoutList)
                    flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                }

                startActivity(intent)
            }
        }
        retrieveFromDb.start()
    }

    private fun registerNotificationChannel() { // Create the NotificationChannel, but only on API 26+ because
        // the NotificationChannel class is new and not in the support library
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val importance = NotificationManager.IMPORTANCE_DEFAULT
            val channel = NotificationChannel(
                NotificationUtil.CHANNEL_ID, NotificationUtil.CHANNEL_NAME, importance
            ).apply {
                description = NotificationUtil.CHANNEL_DESCRIPTION
            }

            // Register the channel with the system
            val notificationManager : NotificationManager =
                getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            notificationManager.createNotificationChannel(channel)
        }
    }
}