package com.example.eventtracker

import android.os.Parcel
import android.os.Parcelable
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

@Entity
data class Payout(
    @PrimaryKey(autoGenerate = true) var id : Int,

    @SerializedName("eventId") @ColumnInfo var payoutId : Int?,

    @SerializedName("status") @ColumnInfo var status : String?,

    @SerializedName("fraudAlert") @ColumnInfo var fraudAlert : Boolean
) : Parcelable {

    constructor(parcel : Parcel) : this(
        parcel.readInt(),
        parcel.readValue(Int::class.java.classLoader) as? Int,
        parcel.readString(),
        parcel.readByte() != 0.toByte()
    ) {
    }

    override fun toString() : String {
        return "Payout [eventId: ${this.payoutId}, status: ${this.status}, fraudAlert: ${this.fraudAlert}]"
    }

    override fun writeToParcel(parcel : Parcel, flags : Int) {
        parcel.writeInt(id)
        parcel.writeValue(payoutId)
        parcel.writeString(status)
        parcel.writeByte(if (fraudAlert) 1 else 0)
    }

    override fun describeContents() : Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Payout> {
        override fun createFromParcel(parcel : Parcel) : Payout {
            return Payout(parcel)
        }

        override fun newArray(size : Int) : Array<Payout?> {
            return arrayOfNulls(size)
        }
    }

}

