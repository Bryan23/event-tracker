package com.example.eventtracker

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.room.Room
import com.example.eventtracker.room.MainDatabase

class PayoutActivity : AppCompatActivity() {
    private lateinit var recyclerView : RecyclerView
    private lateinit var payoutAdapter : PayoutAdapter

    override fun onCreate(savedInstanceState : Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_payout)

        val payouts = intent?.getParcelableArrayListExtra<Payout>("payout")

        payouts?.let {
            recyclerView = findViewById(R.id.recyclerViewPayouts)
            val mLayoutManager = LinearLayoutManager(applicationContext)
            mLayoutManager.orientation = LinearLayoutManager.VERTICAL

            payoutAdapter = PayoutAdapter(it, this)

            with(recyclerView) {
                layoutManager = mLayoutManager
                itemAnimator = DefaultItemAnimator()
                adapter = payoutAdapter
            }

            val saveToDb = Thread {
                val db = Room.databaseBuilder(
                    applicationContext, MainDatabase::class.java, "main-database"
                ).build()

                val payoutDao = db.payoutDao()

                //clear database before inserting
                payoutDao.deleteAll()

                it.forEach { payout -> payoutDao.insertAll(payout) }
            }
            saveToDb.start()
        }
    }
}